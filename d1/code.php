<?php 

// Repetition Control Structure
/*
	While loop
	Do-while loop
	For-loop
*/


// While loop

function whileLoop(){
	$count = 5;

	while($count !== 0){
		echo $count.'<br/>';
		$count--;
	}
}


// Do While Loop
function doWhileLoop(){
	$count = 20;

	do{
		echo $count.'<br/>';
		$count--;
	}while($count > 0);
};


// For loop
function forLoop(){
	for($count =0; $count <=20; $count++){
		echo $count.'<br/>';
	}
}

/*
 Continue and Break Statements 

Continue is a keyword that allows the code to go to the next loop without finishing the current code block. Break keyword ends the execution of the current loop.I 


*/

function modifiedForLoop(){
	for($count =0; $count<=20; $count++){
		if($count % 2 === 0){
			continue;
		}
		echo $count.'<br/>';
		if($count > 10){
			break;
		}
	}
}

// Array Manipulation

// $studentNumbers = array('2020-1923' , '2020-1924','2020-1925','2020-1926','2020-1927');

$studentNumbers = ['2020-1923' , '2020-1924','2020-1925','2020-1926','2020-1927'];


// simple arrays
$grades = [98.5, 94, 96, 93.5, 90.4];
$computerBrands = ['lenovo' ,'acer' , 'asus' ,  'neo', 'hp'];


$task = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];


// Associative Array

$gradePeriods = ['firstGrading' => 95.5,'secondGrading' => 92.5,'thirdGrading' => 94.5,'fourthGrading' => 95.8];


// Two-dimensional array

$heroes = [
	['iron man' , 'thor' , 'hulk'],
	['wolverine' , 'cyclops' , 'storm'],
	['darna' , 'captain barbel' , 'lastikman']
];

$ironManPowers = [
	'regular' => ['repulsor blast' , 'rocket punch'],
	'signature' => ['unibeam']
];

// Array sorting
$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;


sort($sortedBrands);
rsort($reverseSortedBrands);


// other array function

function searchBrand($brands, $brand){
	return (in_array($brand, $brands)) ? "$brand is in the array" : "$brand is not in the array";
}

$reverseGradePeriod = array_reverse($gradePeriods);