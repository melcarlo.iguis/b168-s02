<?php require_once './code.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
	
	<h1>Repetition Control Structures</h1>
	<h2>While loop</h2>
	<p><?php echo whileLoop();?></p>

	<h2>Do-While loop</h2>
	<p><?php echo doWhileLoop();?></p>

	<h2>For loop</h2>
	<p><?php echo forLoop();?></p>


	<h2>Continue and break Statements</h2>
	<p><?php echo modifiedForLoop();?></p>


	<h2>Array manipulation</h2>

	<h2>Types of Array</h2>

	<h2>Simple Array</h2>

	<ul>
		<?php foreach($computerBrands as $brand){?>
			<li><?php echo $brand ?></li>
		<?php }?>
	</ul>


		<h2>Associative Array</h2>

	<ul>
		<?php foreach($gradePeriods as $period => $grade){?>
			<li>Grade in <?= $period ?> is <?= $grade ?></li>
		<?php }?>
	</ul>



	<h2>Multidimensional Array</h2>
	<ul>
	    <?php foreach($heroes as $team){
            foreach($team as $member){
                echo '<li>'.$member.'</li>';
            }
        }?>	
	</ul>


	<h2>Array Functions</h2>

	<h3>Sorting</h3>

	<pre><?php print_r($sortedBrands); ?></pre>
	<pre><?php print_r($reverseSortedBrands); ?></pre>


	<h3>Append</h3>

	<?php array_push($computerBrands, 'apple'); ?>


	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_unshift($computerBrands, 'dell'); ?>


	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Remove</h3>

	<?php array_pop($computerBrands); ?>

	<pre><?php print_r($computerBrands); ?></pre>

	<?php array_shift($computerBrands); ?>

	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Others</h3>

	<h4>Count</h4>

	<pre><?php echo count($computerBrands);?></pre>

	<h4>In Array</h4>

	<p><?php echo searchBrand($computerBrands , 'HP')?></p>

	<h4>Reverse Grade Period</h4>

	<pre><?php print_r($reverseGradePeriod); ?></pre>
</body>
</html>