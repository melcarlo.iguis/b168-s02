<?php require_once './code.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Activity</title>

	<h2>Divisible of Five</h2>
	<p><?php echo numberDivisibleBy5();?></p>

	<h2>Array Manipulation</h2>
	<pre><?php var_dump($studentNames); ?></pre>
	<pre><?php echo count($studentNames);?></pre>

	<?php array_push($studentNames, 'Michael'); ?>
	<pre><?php var_dump($studentNames); ?></pre>
	<pre><?php echo count($studentNames);?></pre>

    <?php array_shift($studentNames); ?>
    <pre><?php var_dump($studentNames); ?></pre>
	<pre><?php echo count($studentNames);?></pre>
</head>
<body>
	
</body>
</html>